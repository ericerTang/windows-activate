package ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;

import common.Static;
import thread.win10.Win10DigitalLicenseActivatie;
import thread.win10.Win10UninstallKms;
import util.MyUitl;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Win10DigitalLicenseJPanel extends JFrame {

	private static final long serialVersionUID = -3419060749767000232L;
	private static volatile Win10DigitalLicenseJPanel instance;

	public JTextArea textArea;
	public JLabel sysLicenseLabel;
	public JButton runActivatieButton;
	public JButton uninstallKmsButton;
	public JProgressBar progressBar;

	private String SKU;

	public Win10DigitalLicenseJPanel() {

		JPanel panel = new JPanel();
		panel.setLayout(null);
		getContentPane().add(panel, BorderLayout.CENTER);

		/* 系统信息 */
		JPanel sysInfoPanel = new JPanel();
		sysInfoPanel.setBorder(
				new TitledBorder(null, "\u7CFB\u7EDF\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sysInfoPanel.setBounds(10, 10, 374, 145);
		panel.add(sysInfoPanel);
		sysInfoPanel.setLayout(null);

		// 系统类型
		JLabel sysEditionLabel = new JLabel("");
		sysEditionLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		sysEditionLabel.setBounds(15, 25, 349, 18);
		sysInfoPanel.add(sysEditionLabel);

		// 系统版本
		JLabel sysVersionLabel = new JLabel("");
		sysVersionLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		sysVersionLabel.setBounds(15, 53, 349, 18);
		sysInfoPanel.add(sysVersionLabel);

		// 激活状态
		sysLicenseLabel = new JLabel("\u6FC0\u6D3B\u72B6\u6001\uFF1A\u6B63\u5728\u83B7\u53D6...");
		sysLicenseLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		sysLicenseLabel.setBounds(15, 81, 349, 18);
		sysInfoPanel.add(sysLicenseLabel);

		// sku
		JLabel sysSkuLabel = new JLabel("");
		sysSkuLabel.setBounds(15, 109, 316, 18);
		sysInfoPanel.add(sysSkuLabel);
		sysSkuLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		/* -END- 系统信息 */

		JPanel activatieProcessPanel = new JPanel();
		activatieProcessPanel.setBorder(
				new TitledBorder(null, "\u6FC0\u6D3B\u8FDB\u5EA6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		activatieProcessPanel.setBounds(10, 165, 374, 209);
		activatieProcessPanel.setLayout(new BorderLayout(0, 0));
		panel.add(activatieProcessPanel);

		JScrollPane scrollPane = new JScrollPane();
		activatieProcessPanel.add(scrollPane, BorderLayout.CENTER);

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
		
		JLabel uninstallKmsLabel = new JLabel(
				"\u82E5\u4E4B\u524D\u4E3Akms\u6FC0\u6D3B\uFF0C\u9700\u5148\u5378\u8F7Dkms");
		uninstallKmsLabel.setBounds(10, 384, 269, 27);
		panel.add(uninstallKmsLabel);
		uninstallKmsLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));

		uninstallKmsButton = new JButton("\u5378\u8F7DKMS");
		uninstallKmsButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == 1 && uninstallKmsButton.isEnabled()) {
					new Thread(new Win10UninstallKms()).start();
				}
			}
		});
		uninstallKmsButton.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		uninstallKmsButton.setBounds(289, 384, 95, 27);
		panel.add(uninstallKmsButton);

		runActivatieButton = new JButton("\u6FC0\u6D3B");
		runActivatieButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == 1 && runActivatieButton.isEnabled()) {
					new Thread(new Win10DigitalLicenseActivatie(SKU)).start();
				}
			}
		});
		runActivatieButton.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		runActivatieButton.setFocusPainted(false);
		runActivatieButton.setBounds(309, 420, 75, 27);
		runActivatieButton.setEnabled(false);
		panel.add(runActivatieButton);

		progressBar = new JProgressBar();
		progressBar.setBounds(10, 421, 289, 26);
		panel.add(progressBar);

		/* 获取系统信息 */
		try {
			String sysEdition = MyUitl.getSysEdition();
			String sysVersion = MyUitl.getSysVersion();
			sysEditionLabel.setText("系统类型：" + sysEdition);
			sysVersionLabel.setText("系统版本：" + sysVersion);
			String sysSku = Static.WIN10_DIGITAL_LICENSE_SKU.get(sysEdition);
			if (sysSku != null) {
				SKU = sysSku;
				if (Static.WIN10_DIGITAL_LICENSE_KEY.get(sysSku) != null) {
					runActivatieButton.setEnabled(true);
					sysSkuLabel.setText("SKU：" + sysSku);
				} else {
					sysSkuLabel.setText("SKU：" + sysSku + "（该版本暂未支持）");
				}
			}
		} catch (IOException e) {
			sysEditionLabel.setText("系统类型获取失败");
			sysVersionLabel.setText("系统版本获取失败");
			sysSkuLabel.setText("SKU获取失败");
			e.printStackTrace();
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String sysLicenseValue = MyUitl.getActivatieStatus();
					if (!"".equals(sysLicenseValue)) {
						sysLicenseLabel.setText("激活状态：" + sysLicenseValue);
					} else {
						sysLicenseLabel.setText("激活状态：未找到密钥");
					}
				} catch (Exception e) {
					sysLicenseLabel.setText("激活状态：获取失败");
					e.printStackTrace();
				}
			}
		}).start();
		/* -END- 获取系统信息 */

		this.setTitle("Windows 10 \u6570\u5B57\u8BB8\u53EF\u6FC0\u6D3B");
		this.setSize(new Dimension(400, 485));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - this.getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - this.getHeight()) / 2);
		this.setVisible(true);
		this.setResizable(false);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	public void consoleLog(String s) {
		textArea.append(s + "\r\n");
		textArea.setCaretPosition(textArea.getText().length());
	}

	public static Win10DigitalLicenseJPanel getIstance() {
		if (instance == null) {
			synchronized (Win10DigitalLicenseJPanel.class) {
				if (instance == null) {
					instance = new Win10DigitalLicenseJPanel();
				}
			}
		}
		return instance;
	}
}
